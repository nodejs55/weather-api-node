const rp = require('request-promise')

module.exports = async function(city = '') {
    if (!city) {
        throw new Error('City not found')
    }

    const KEY = 'bfc5338f58dbc5216ff976b352f57d9d'
    const uri = 'http://api.openweathermap.org/data/2.5/weather'

    const options = {
        uri,
        qs: {
            appid: KEY,
            q: city,
            units: 'imperial'
        },
        json: true
    }

    try {
        const data = await rp(options)
        console.log(data);
        const celsius = (data.main.temp - 32) * 5/9

        return {
            weather: `${data.name}: ${celsius.toFixed(0)}`,
            error: null
        }

    } catch(error) {
        console.log(error)
        return {
            weather: null,
            error: error.error.message
        }

    }


}